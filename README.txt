Name: 		Darren Silke
Student Number: SLKDAR001
Date:           27 July 2015

Game Name: Equilibrium - A Level Prototype

Description:

Use the up arrow key to make the player run forward and the mouse to control the player's rotation.
Use right shift + up arrow key to make the player sneak.
Press x to make the player shout.
Press end to make the player die and restart the level.
To raise the player's weapon and aim, hold down the right mouse button.
To shoot, press the left mouse button.

To switch between the different camera modes, press tab.
While in third person camera mode, the camera can be adjusted by using the scroll wheel to zoom in and out and the u and d buttons to move the camera up and down respectively.
While in orbit camera mode, the camera can be adjusted by using the scroll wheel to zoom in and out. The camera is controlled through the movement of the mouse.

Instructions:

1. Run the game using the packaged .exe file in an appropriate widescreen resolution.
2. Follow the on-screen instructions in terms of clicking on the appropriate buttons.
3. To exit the game, click the cross at the top right corner of the window.