using UnityEngine;
using System.Collections;

public class CameraSwitcher : MonoBehaviour
{	
		private GameObject mainCamera;                       // Reference to the main camera.
		private Transform[] cameraTargets;                   // Reference to the camera targets.
		private MainCameraController mainCameraController;   // Reference to the MainCameraController script.
		private OrbitCameraController orbitCameraController; // Reference to the OrbitCameraController script.
		private CameraAdjust cameraAdjust;                   // Reference to the CameraAdjust script.
		private PlayerController playerMovement;		     // Reference to the PlayerMovement script.
		private int cameraIndex;                             // An int to keep track of the current camera mode.
	
		void Awake ()
		{
				// Setting up the references.
				mainCamera = GameObject.FindGameObjectWithTag (Tags.mainCamera);
				mainCameraController = mainCamera.GetComponent<MainCameraController> ();
				orbitCameraController = GameObject.FindGameObjectWithTag (Tags.orbitCamera).GetComponent<OrbitCameraController> ();
				cameraTargets = mainCameraController.cameraTargets;
				playerMovement = GameObject.FindGameObjectWithTag (Tags.player).GetComponent<PlayerController> ();
				cameraAdjust = GameObject.FindGameObjectWithTag (Tags.player).GetComponentInChildren<CameraAdjust> ();
		}

		void Update ()
		{
				if (Input.GetButtonDown ("Switch Camera")) {
						cameraIndex++;

						if (cameraIndex == cameraTargets.Length) {
								mainCamera.SetActive (false);
								playerMovement.enabled = false;
								orbitCameraController.canRotate = true;
						}

						if (cameraIndex > cameraTargets.Length) {				
								mainCameraController.cameraIndex = 0;
								mainCameraController.mainCameraSettings = cameraTargets [mainCameraController.cameraIndex].GetComponent<MainCameraSettings> ();
								mainCamera.SetActive (true);
								playerMovement.enabled = true;
								orbitCameraController.canRotate = false;
								cameraIndex = 0;
						}

						if (cameraIndex == 0) {
								cameraAdjust.canAdjust = true;
						} else {
								cameraAdjust.canAdjust = false;
						}
				}
		}
}
