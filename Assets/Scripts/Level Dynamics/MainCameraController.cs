using UnityEngine;
using System.Collections;

public class MainCameraController : MonoBehaviour
{
		public Transform[] cameraTargets;
		public int cameraIndex;
		public MainCameraSettings mainCameraSettings;
		private float cameraRotX = 0f;

		void Awake ()
		{
				mainCameraSettings = cameraTargets [cameraIndex].GetComponent<MainCameraSettings> ();
		}

		void Update ()
		{
				if (Input.GetButtonDown ("Switch Camera")) {
						cameraIndex++;

						if (cameraIndex >= cameraTargets.Length)
								cameraIndex = 0;
			
						mainCameraSettings = cameraTargets [cameraIndex].GetComponent<MainCameraSettings> ();
				}
		
				if (cameraTargets [cameraIndex]) {
						if (mainCameraSettings.smoothing == 0f) {
								transform.position = cameraTargets [cameraIndex].position;	
								transform.rotation = cameraTargets [cameraIndex].rotation;	
						} else {
								transform.position = Vector3.Lerp (transform.position, cameraTargets [cameraIndex].position, Time.deltaTime * mainCameraSettings.smoothing);
								transform.rotation = cameraTargets [cameraIndex].rotation;
						}

						cameraRotX -= Input.GetAxis ("Mouse Y");
		
						cameraRotX = Mathf.Clamp (cameraRotX, -mainCameraSettings.cameraPitchMax, mainCameraSettings.cameraPitchMax);
		
						Camera.main.transform.Rotate (cameraRotX, 0f, 0f);
				}
		}
}
