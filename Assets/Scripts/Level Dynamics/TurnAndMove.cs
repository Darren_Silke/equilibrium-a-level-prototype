﻿using UnityEngine;
using System.Collections;

public class TurnAndMove : MonoBehaviour
{
		public float angleX = 45f;
		public float angleY = 45f;
		public float angleZ = 45f;

		void Update ()
		{
				transform.Rotate (new Vector3 (1, 0, 0) * angleX * Time.deltaTime);
		}
}
