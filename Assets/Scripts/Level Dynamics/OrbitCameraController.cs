﻿using UnityEngine; 
using System.Collections;

public class OrbitCameraController : MonoBehaviour
{ 
		public Transform target;
		public float distance = 10.0f;
		public float xSpeed = 250.0f;
		public float ySpeed = 120.0f;
		public float yMinLimit = -20f;
		public float yMaxLimit = 80f;
		public bool canRotate;
		private float x = 0.0f;
		private float y = 0.0f;

		void Awake ()
		{
				Vector3 angles = transform.eulerAngles;
				x = angles.y;
				y = angles.x;
				canRotate = false;
		}
	
		void LateUpdate ()
		{
				if (target) {
						if (canRotate) {
								x += (float)(Input.GetAxis ("Mouse X") * xSpeed * 0.02);
								y += (float)(Input.GetAxis ("Mouse Y") * ySpeed * 0.02);
								distance -= (float)(Input.GetAxis ("Mouse Scroll Wheel"));
						}

						y = ClampAngle (y, yMinLimit, yMaxLimit);
			
						Quaternion rotation = Quaternion.Euler (y, x, 0);
						Vector3 position = rotation * new Vector3 (0.0f, 0.0f, -distance) + target.position;
			
						transform.rotation = rotation;
						transform.position = position;
				}
		}
	
		private int ClampAngle (float angle, float min, float max)
		{
				if (angle < -360) {
						angle += 360;
				}

				if (angle > 360) {
						angle -= 360;
				}

				return Mathf.Clamp ((int)(angle), (int)(min), (int)(max));
		}
}
