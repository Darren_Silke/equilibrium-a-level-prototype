﻿using UnityEngine;
using System.Collections;

public class CameraAdjust : MonoBehaviour
{	
		public bool canAdjust;								// A bool to indicate whether the camera is adjustable or not.

		void Awake ()
		{
				canAdjust = true;
		}

		void Update ()
		{
				// Perform necessary camera adjustments on the camera if canAdjust is true.
				if (canAdjust) {
						if ((float)Input.GetAxis ("Mouse Scroll Wheel") != 0f)
								transform.Translate (0f, 0f, (float)Input.GetAxis ("Mouse Scroll Wheel"));
						if (Input.GetButtonDown ("Camera Up"))
								transform.Translate (0f, 0.05f, 0f);
						if (Input.GetButtonDown ("Camera Down"))
								transform.Translate (0f, -0.05f, 0f);
				}
		}
}
