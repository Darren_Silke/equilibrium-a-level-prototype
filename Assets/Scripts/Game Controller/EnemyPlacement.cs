﻿using UnityEngine;
using System.Collections;

public class EnemyPlacement : MonoBehaviour
{
		public GameObject enemyPrefab;		// A reference to an enemy prefab to be instantiated into the scene.

		private ArrayList spawnPoints;      // An array list of possible spawn locations for the enemies.
		private ArrayList yRotations;       // An array list to store the appropriate rotation configurations for the enemies.
		private ArrayList colors;           // An array list to store colors to assign to the enemies.

		void Awake ()
		{
				// Setting up the array list of colors.
				colors = new ArrayList ();
				colors.Capacity = 10;
				colors.Add (Color.blue);
				colors.Add (Color.cyan);
				colors.Add (Color.gray);
				colors.Add (Color.green);
				colors.Add (Color.magenta);
				colors.Add (Color.red);
				colors.Add (Color.yellow);
				colors.Add (Color.Lerp (Color.red, Color.white, 0.5f));
				colors.Add (Color.Lerp (Color.red, Color.yellow, 0.5f));
				colors.Add (Color.Lerp (Color.yellow, Color.green, 0.5f));

				// Setting up the array list of spawn points and rotation configurations.
				spawnPoints = new ArrayList ();
				spawnPoints.Capacity = 20;
				yRotations = new ArrayList ();
				yRotations.Capacity = 20;
				spawnPoints.Add (new Vector3 (-21.50f, 0f, 2.86f));
				yRotations.Add (180f);
				spawnPoints.Add (new Vector3 (-18.13f, 0f, 8.76f));
				yRotations.Add (180f);
				spawnPoints.Add (new Vector3 (-17.12f, 0f, -5.30f));
				yRotations.Add (0f);
				spawnPoints.Add (new Vector3 (-11.27f, 0f, 8.46f));
				yRotations.Add (180f);
				spawnPoints.Add (new Vector3 (-13.93f, 0f, 3.12f));
				yRotations.Add (270f);
				spawnPoints.Add (new Vector3 (-12.15f, 0f, 0.34f));
				yRotations.Add (0f);
				spawnPoints.Add (new Vector3 (-20.94f, 0f, -6.71f));
				yRotations.Add (0f);
				spawnPoints.Add (new Vector3 (-16.40f, -1f, 1.28f));
				yRotations.Add (270f);
				spawnPoints.Add (new Vector3 (-23.25f, 0f, 0.45f));
				yRotations.Add (90f);
				spawnPoints.Add (new Vector3 (-17.21f, 0f, 3.46f));
				yRotations.Add (90f);
				spawnPoints.Add (new Vector3 (-19.24f, -1f, -2.88f));
				yRotations.Add (270f);
				spawnPoints.Add (new Vector3 (-19.43f, -1f, -5.25f));
				yRotations.Add (90f);
				spawnPoints.Add (new Vector3 (-20.99f, -1f, 0.56f));
				yRotations.Add (180f);
				spawnPoints.Add (new Vector3 (-17.23f, -1f, -6.97f));
				yRotations.Add (0f);
				spawnPoints.Add (new Vector3 (-15.16f, 0f, 8.60f));
				yRotations.Add (180f);
				spawnPoints.Add (new Vector3 (-12.49f, 0f, 3.43f));
				yRotations.Add (90f);
				spawnPoints.Add (new Vector3 (-20.25f, 0f, 2.26f));
				yRotations.Add (270f);
				spawnPoints.Add (new Vector3 (-19.59f, 0f, 4.43f));
				yRotations.Add (0f);
				spawnPoints.Add (new Vector3 (-17.36f, -1f, -4.06f));
				yRotations.Add (90f);
				spawnPoints.Add (new Vector3 (-21.18f, 0f, 8.60f));
				yRotations.Add (180f);
	
				// Assigning random spawn locations and colors to 5 enemies.
				int randomPosition;
				GameObject enemy;
				Renderer[] renderers;
				for (int i = 0; i < 5; i++) {
						randomPosition = Random.Range (0, spawnPoints.Count);
						enemy = Instantiate (enemyPrefab, (Vector3)spawnPoints [randomPosition], Quaternion.AngleAxis ((float)yRotations [randomPosition], Vector3.up)) as GameObject;

						spawnPoints.RemoveAt (randomPosition);
						yRotations.RemoveAt (randomPosition);

						renderers = enemy.GetComponentsInChildren<Renderer> ();
						randomPosition = Random.Range (0, colors.Count);

						for (int j = 0; j < renderers.Length; j++)
								renderers [j].material.color = (Color)colors [randomPosition];

						colors.RemoveAt (randomPosition);
				}
		}
}
