﻿using UnityEngine;
using System.Collections;

public class HashIDs : MonoBehaviour
{
		// Here, the hash tags are stored for various strings used in the animators.
		public int dyingState;
		public int locomotionState;
		public int shoutState;
		public int deadBool;
		public int speedFloat;
		public int sneakingBool;
		public int shoutingBool;
		public int shotFloat;
		public int weaponRaiseBool;
		public int weaponShootBool;
	
		void Awake ()
		{
				dyingState = Animator.StringToHash ("Base Layer.Dying");
				locomotionState = Animator.StringToHash ("Base Layer.Locomotion");
				shoutState = Animator.StringToHash ("Shouting.Shout");
				deadBool = Animator.StringToHash ("Dead");
				speedFloat = Animator.StringToHash ("Speed");
				sneakingBool = Animator.StringToHash ("Sneaking");
				shoutingBool = Animator.StringToHash ("Shouting");
				shotFloat = Animator.StringToHash ("Shot");
				weaponRaiseBool = Animator.StringToHash ("WeaponRaise");
				weaponShootBool = Animator.StringToHash ("WeaponShoot");
		}
}
