﻿using UnityEngine;
using System.Collections;

public class PlayerShooting : MonoBehaviour
{
		public AudioClip shotClip;                          // An audio clip to play when a shot happens.
		public float flashIntensity = 3f;                   // The intensity of the light when the shot happens.
		public float fadeSpeed = 10f;                       // How fast the light will fade after the shot.
		public ParticleSystem explosionEffect;              // A particle system for the explosion effect when an enemy is killed.
		public AudioClip explosionClip;                     // An audio clip to play when the explosion effect happens.

		private Animator anim;                              // Reference to the animator.
		private HashIDs hash;                               // Reference to the HashIDs script.
		private LineRenderer laserShotLine;                 // Reference to the laser shot line renderer.
		private Light laserShotLight;                       // Reference to the laser shot light.
		private bool shooting;                              // A bool to say whether or not the player is currently shooting.
		private TextMesh textMesh;                          // Reference to the TextMesh component.

		void Awake ()
		{
				// Setting up the references.
				anim = GetComponent<Animator> ();
				laserShotLine = GetComponentInChildren<LineRenderer> ();
				laserShotLight = laserShotLine.gameObject.light;
				hash = GameObject.FindGameObjectWithTag (Tags.gameController).GetComponent<HashIDs> ();
				textMesh = gameObject.GetComponentInChildren<TextMesh> ();
		
				// The line renderer and light are off to start.
				laserShotLine.enabled = false;
				laserShotLight.intensity = 0f;
		}
	
		void Update ()
		{
				// Cache the current value of the shot curve.
				float shot = anim.GetFloat (hash.shotFloat);
		
				// If the shot curve is peaking and the player is not currently shooting...
				if (shot > 0.5f && !shooting)
			// ... shoot
						Shoot ();
		
				// If the shot curve is no longer peaking...
				if (shot < 0.5f) {
						// ... the player is no longer shooting and disable the line renderer.
						shooting = false;
						laserShotLine.enabled = false;
				}
		
				// Fade the light out.
				laserShotLight.intensity = Mathf.Lerp (laserShotLight.intensity, 0f, fadeSpeed * Time.deltaTime);
		}

		void Shoot ()
		{
				// The player is shooting.
				shooting = true;

				Vector3 direction = textMesh.transform.position - laserShotLine.transform.position;
				float distance = Vector3.Distance (textMesh.transform.position, laserShotLine.transform.position) + 1.5f;

				RaycastHit hit;
				if (Physics.Raycast (laserShotLine.transform.position + transform.up, direction.normalized, out hit, distance)) {
						if (hit.collider.gameObject.tag == Tags.enemy) {
								Destroy (hit.collider.gameObject);
								ParticleSystem newParticleSystem = Instantiate (explosionEffect, hit.collider.gameObject.transform.position, Quaternion.identity) as ParticleSystem;
								Destroy (newParticleSystem.gameObject, newParticleSystem.startLifetime);
								AudioSource.PlayClipAtPoint (explosionClip, hit.collider.gameObject.transform.position);
						}
				}

				// Display the shot effects.
				ShotEffects ();

		}
	
		void ShotEffects ()
		{
				// Set the initial position of the line renderer to the position of the muzzle.
				laserShotLine.SetPosition (0, laserShotLine.transform.position);
		
				// Set the end position of the line renderer to the position of the crosshair.
				laserShotLine.SetPosition (1, textMesh.gameObject.transform.position);
		
				// Turn on the line renderer.
				laserShotLine.enabled = true;
		
				// Make the light flash.
				laserShotLight.intensity = flashIntensity;
		
				// Play the gun shot clip at the position of the muzzle flare.
				AudioSource.PlayClipAtPoint (shotClip, laserShotLight.transform.position);
		}
}
