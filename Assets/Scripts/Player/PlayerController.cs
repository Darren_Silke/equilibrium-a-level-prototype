﻿using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour
{
		public AudioClip shoutingClip;      // Audio clip of the player shouting.
		public float speedDampTime = 0.1f;  // The damping for the speed parameter.
		public float turnSpeed = 180f;      // The turn speed for when the player is turning.

		private Animator anim;              // Reference to the animator component.
		private HashIDs hash;               // Reference to the HashIDs.
		private TextMesh textMesh;          // Reference to the text mesh component.
		private PlayerHealth playerHealth;  // Reference to the PlayerHealth script.
	
		void Awake ()
		{
				// Setting up the references.
				anim = GetComponent<Animator> ();
				hash = GameObject.FindGameObjectWithTag (Tags.gameController).GetComponent<HashIDs> ();
				textMesh = gameObject.GetComponentInChildren<TextMesh> ();
				playerHealth = GetComponent<PlayerHealth> ();

				// Set the weight of the shooting layer to 1.
				anim.SetLayerWeight (1, 1f);

				// Set the weight of the gun layer to 1.
				anim.SetLayerWeight (2, 1f);

				// Set the weight of the shouting layer to 1.
				anim.SetLayerWeight (3, 1f);
		}
	
		void FixedUpdate ()
		{
				// Cache the inputs.
				float v = Input.GetAxis ("Vertical");
				bool sneak = Input.GetButton ("Sneak");
		
				MovementManagement (v, sneak);
		}
	
		void Update ()
		{
				// Cache the attention attracting input.
				bool shout = Input.GetButtonDown ("Attract");
				// Set the animator shouting parameter.
				anim.SetBool (hash.shoutingBool, shout);
				AudioManagement (shout);

				bool weaponRaise = Input.GetButton ("Weapon Raise");
				// Set the animator weaponRaise parameter.
				anim.SetBool (hash.weaponRaiseBool, weaponRaise);
				textMesh.renderer.enabled = weaponRaise;

				bool weaponShoot = Input.GetButton ("Weapon Shoot");
				// Set the animator weaponShoot parameter.
				anim.SetBool (hash.weaponShootBool, weaponShoot);

				if (Input.GetButtonDown ("Die"))
						playerHealth.TakeDamage (100f);	
		}
	
		void MovementManagement (float vertical, bool sneaking)
		{
				// Set the sneaking parameter to the sneak input.
				anim.SetBool (hash.sneakingBool, sneaking);
		
				// If there is some axis input...
				if (vertical != 0f) {
						// ... set the player's speed parameter to 5f.
						anim.SetFloat (hash.speedFloat, 5f, speedDampTime, Time.deltaTime);
				} else
			// Otherwise set the speed parameter to 0.
						anim.SetFloat (hash.speedFloat, 0f);

				transform.Rotate (0f, Input.GetAxis ("Mouse X") * turnSpeed * Time.deltaTime, 0f);
		}
		
		void AudioManagement (bool shout)
		{
				// If the player is currently in the run state...
				if (anim.GetCurrentAnimatorStateInfo (0).nameHash == hash.locomotionState) {
						// ... and if the footsteps are not playing...
						if (!audio.isPlaying)
				// ... play them.
								audio.Play ();
				} else
			// Otherwise stop the footsteps.
						audio.Stop ();
		
				// If the shout input has been pressed...
				if (shout)
			// ... play the shouting clip where the player is.
						AudioSource.PlayClipAtPoint (shoutingClip, transform.position);
		}
}
